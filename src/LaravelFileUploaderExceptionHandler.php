<?php


namespace keithe\LaravelFileUploader;

use Exception;
use keithe\FileUploader\Exceptions\DirectoryNotFoundException;
use keithe\FileUploader\Exceptions\FileSizeTooLargeException;
use keithe\FileUploader\Exceptions\InvalidFileTypeException;
use keithe\FileUploader\Exceptions\NoOverwritePermissionException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class LaravelFileUploaderExceptionHandler
{
    protected $exception;
    protected $error;

    function __construct(Exception $e)
    {
        $this->exception = $e;
    }


    public function getUploadErrors()
    {
        if ($this->exception instanceof DirectoryNotFoundException) {
            $this->error = trans('laravel-file-uploader::exceptions.directoryNotFound');
            return $this->response();
        }
        if ($this->exception instanceof FileSizeTooLargeException) {
            $this->error = trans('laravel-file-uploader::exceptions.fileSizeTooLarge');
            return $this->response();
        }
        if ($this->exception instanceof InvalidFileTypeException) {
            $this->error = trans('laravel-file-uploader::exceptions.invalidFileType');
            return $this->response();
        }
        if ($this->exception instanceof NoOverwritePermissionException) {
            $this->error = trans('laravel-file-uploader::exceptions.noOverwritePermission');
            return $this->response();
        }

        return false;
    }

    /**
     * The response when exceptions are thrown
     * @return mixed
     */
    protected function response()
    {
        return Redirect::back()->withInput()->withErrors($this->error);
    }

}